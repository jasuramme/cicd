#Этот файл должен запускаться отдельно с ключом -DCMAKE_TOOLCHAIN_FILE=
#Так как CMAKE будет проверять возможности системы до того, как обратится
#к CMakeLists.txt

set(CMAKE_SYSTEM_NAME  Generic)
set(CMAKE_SYSTEM_PROCESSOR ARM)
set(CMAKE_CROSSCOMPILING 1)
set(CMAKE_TRY_COMPILE_TARGET_TYPE STATIC_LIBRARY)

set(CMAKE_C_COMPILER arm-none-eabi-gcc)
set(CMAKE_CXX_COMPILER arm-none-eabi-g++)
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)

set(OPTIMIZATION_FLAGS "-Og -g3 -DDebug -DTRACE -DOS_USE_TRACE_SEMIHOSTING_DEBUG")
#-DOS_USE_TRACE_SEMIHOSTING_DEBUG"
IF(CMAKE_BUILD_TYPE STREQUAL "Release")
    set(OPTIMIZATION_FLAGS "-Os -g3")
ENDIF(CMAKE_BUILD_TYPE STREQUAL "Release")

set(CMAKE_C_COMPILER arm-none-eabi-gcc)
set(CMAKE_CXX_COMPILER arm-none-eabi-g++)
set(CMAKE_ASM_COMPILER arm-none-eabi-gcc)

#-ffunction-sections -fdata-sections -nostartfiles
set(WARNING_FLAGS "-Wall")
set(ARCH_FLAGS "-mcpu=cortex-m4 -mthumb -mfpu=fpv4-sp-d16 -mfloat-abi=hard")
set(SHARED_FLAGS "-ffreestanding -flto -MMD -MP ")
set(LINKER_OPTS "-fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-move-loop-invariants")
set(DEFINES "-DUSE_HAL_DRIVER -DSTM32F407xx -DHSE_VALUE=8000000 -DUSE_FULL_ASSERT")
set(GCC_COVERAGE_COMPILE_FLAGS "${ARCH_FLAGS} ${SHARED_FLAGS} ${DEFINES} ${OPTIMIZATION_FLAGS}")

set(CMAKE_C_FLAGS "${GCC_COVERAGE_COMPILE_FLAGS}")
set(CMAKE_CXX_FLAGS "${GCC_COVERAGE_COMPILE_FLAGS}")
SET(CMAKE_ASM_FLAGS "${GCC_COVERAGE_COMPILE_FLAGS} -x assembler-with-cpp -c")

set(LINKER_SCRIPTS "-L\"../../ldscripts\" -T libs.ld -T mem.ld -T sections.ld")
set(CMAKE_EXE_LINKER_FLAGS "${GCC_COVERAGE_COMPILE_FLAGS} ${LINKER_SCRIPTS} -specs=nano.specs -lc -lm -Wl,-Map=${CMAKE_PROJECT_NAME}.map,--cref -Wl,--gc-sections")
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)
