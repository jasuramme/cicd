SET (CMSIS_SOURCE_FILES
./system/src/cmsis/system_stm32f4xx.c
./system/src/cmsis/vectors_stm32f407xx.c
./system/src/cortexm/_initialize_hardware.c
./system/src/cortexm/_reset_hardware.c
./system/src/cortexm/exception_handlers.c
)


SET (CMSIS_HEADER_DIRS
./system/include/arm
./system/include/cortexm
./system/include/cmsis
./system/include
)
